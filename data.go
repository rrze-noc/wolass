// Data handling

// Copyright (C) 2022  Simon Ruderich
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/json"
	"os"
	"path/filepath"

	"github.com/google/renameio/v2"
)

type Data struct {
	NextId int    `json:"nextId"`
	Hosts  []Host `json:"hosts"`
}

type Host struct {
	Id      int    `json:"id"`
	Network string `json:"network"` // in CIDR notation
	Name    string `json:"name"`
	MAC     string `json:"mac"`
	Group   string `json:"group"`
}

func LoadData(path string) (*Data, error) {
	x, err := os.ReadFile(path)
	if err != nil {
		if !os.IsNotExist(err) {
			return nil, err
		}
		x = []byte("{}")
	}
	var res Data
	err = json.Unmarshal(x, &res)
	if err != nil {
		return nil, err
	}

	// Defaults
	if res.NextId == 0 {
		res.NextId = 1
	}

	return &res, nil
}

func SaveData(path string, data *Data) error {
	dir := filepath.Dir(path)

	x, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return err
	}
	f, err := renameio.TempFile(dir, path)
	if err != nil {
		return err
	}
	defer f.Cleanup() //nolint:errcheck
	_, err = f.Write(x)
	if err != nil {
		return err
	}
	err = f.CloseAtomicallyReplace()
	if err != nil {
		return err
	}

	err = FsyncDir(dir)
	if err != nil {
		return err
	}
	return nil
}

func FsyncDir(path string) error {
	// To guarantee durability fsync must be called on a parent directory
	// after adding, renaming or removing files therein.
	//
	// Calling sync on the files itself is not enough according to POSIX;
	// man 2 fsync: "Calling fsync() does not necessarily ensure that the
	// entry in the directory containing the file has also reached disk.
	// For that an explicit fsync() on a file descriptor for the directory
	// is also needed."
	d, err := os.Open(path)
	if err != nil {
		return err
	}
	err = d.Sync()
	cerr := d.Close()
	if err != nil {
		return err
	}
	return cerr
}
