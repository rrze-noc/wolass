// Copyright (C) 2022  Simon Ruderich
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

'use strict';


async function load() {
    const resp = await fetch('api/load', {
        redirect: 'manual',
    });
    // Redirect indicates the user needs to login via SSO; reload page as
    // simplest solution
    if (!resp.ok && resp.type === 'opaqueredirect') {
        location.reload();
        return;
    }

    const json = await resp.json();
    if (!resp.ok) {
        // Server sends message in "error"
        alert(json.error); // TODO: better error handling
        return;
    }

    document.querySelector('#user').textContent = json.user;

    const networks = document.querySelector('#add-network');
    networks.innerHTML = ''; // delete all children
    for (const x of json.networks) {
        const option = document.createElement('option');
        option.textContent = x;
        networks.appendChild(option);
    }

    const noHosts = json.hosts.length === 0;
    const noNetworks = json.networks.length === 0;
    document.querySelector('#warning').hidden = !(noHosts && noNetworks);
    document.querySelector('#hosts-list').hidden = noHosts;
    document.querySelector('#hosts-add').hidden = noNetworks;

    // Group hosts with the same group together
    const groups = new Map();
    for (const x of json.hosts) {
        if (!groups.has(x.group)) {
            groups.set(x.group, []);
        }
        groups.get(x.group).push(x);
    }

    const hosts = document.querySelector('#hosts');
    hosts.innerHTML = ''; // delete all children
    for (const group of [...groups.keys()].sort()) {
        // Group name
        if (group !== '') {
            const h = document.createElement('h3');
            h.textContent = group;
            hosts.appendChild(h);
        }
        const ul = document.createElement('ul');
        hosts.appendChild(ul);

        for (const x of groups.get(group)) {
            const indicator = document.createElement('strong');

            const wake = document.createElement('a');
            wake.href = '#';
            wake.textContent = x.name;
            wake.onclick = (_) => {
                hostWake(x, indicator);
            };

            const del = document.createElement('a');
            del.href = '#';
            del.textContent = 'Delete';
            del.onclick = (_) => {
                hostDelete(x);
            };
            const delSmall = document.createElement('small');
            delSmall.appendChild(del);

            const li = document.createElement('li');
            li.appendChild(wake);
            li.appendChild(indicator);
            li.appendChild(document.createTextNode(' ('));
            li.appendChild(document.createTextNode(x.network));
            li.appendChild(document.createTextNode(', '));
            li.appendChild(document.createTextNode(x.mac));
            li.appendChild(document.createTextNode(') '));
            if (x.admin) {
                li.appendChild(delSmall);
            }

            ul.appendChild(li);
        }
    }
}


async function hostAdd(evt) {
    // Don't actually submit form
    evt.preventDefault();

    const network = document.querySelector('#add-network').value;
    const name = document.querySelector('#add-name').value;
    const mac = document.querySelector('#add-mac').value.trim();
    const group = document.querySelector('#add-group').value.trim();

    const data = {
        host: {
            network: network,
            name: name,
            mac: mac,
            group: group,
        },
    };
    const resp = await fetch('api/add', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        redirect: 'manual',
    });
    if (!resp.ok && resp.type === 'opaqueredirect') {
        location.reload();
        return;
    }
    const json = await resp.json();
    if (!resp.ok) {
        // Server sends message in "error"
        alert(json.error); // TODO: better error handling
        return;
    }

    // Don't reset "network" to not confuse the user
    document.querySelector('#add-name').value = '';
    document.querySelector('#add-mac').value = '';
    document.querySelector('#add-group').value = '';

    // Simplest way to fetch latest version
    load();
}

async function hostWake(host, indicator) {
    indicator.textContent = ' Please wait ';

    const data = {
        hostId: host.id,
    };
    const resp = await fetch('api/wake', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        redirect: 'manual',
    });
    if (!resp.ok && resp.type === 'opaqueredirect') {
        location.reload();
        return;
    }
    const json = await resp.json();
    if (!resp.ok) {
        // Server sends message in "error"
        alert(json.error); // TODO: better error handling
        return;
    }

    indicator.textContent = ' WOL-packet sent';
}

async function hostDelete(host) {
    if (!confirm(`Really delete host "${host.name}"?`)) {
        return;
    }

    const data = {
        hostId: host.id,
    };
    const resp = await fetch('api/delete', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        redirect: 'manual',
    });
    if (!resp.ok && resp.type === 'opaqueredirect') {
        location.reload();
        return;
    }
    const json = await resp.json();
    if (!resp.ok) {
        // Server sends message in "error"
        alert(json.error); // TODO: better error handling
        return;
    }

    // Simplest way to fetch latest version
    load();
}


document.querySelector('#add-submit').onclick = hostAdd;

load();
