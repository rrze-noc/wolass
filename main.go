// Wake-on-LAN as a service wakes remote hosts with WOL packets

// Copyright (C) 2022  Simon Ruderich
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strings"
	"sync"
)

//go:embed static/*
var static embed.FS

func main() {
	if len(os.Args) != 2 {
		log.SetFlags(0)
		log.Fatalf("usage: %s <config.toml>", os.Args[0])
	}

	cfg, err := LoadConfig(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	samlSP, err := newSamlSP(cfg)
	if err != nil {
		log.Fatal(err)
	}

	requireSaml := func(
		f func(http.ResponseWriter, *http.Request)) http.Handler {
		return samlSP.RequireAccount(http.HandlerFunc(f))
	}

	// Use existing static/ directory for quick changes
	var staticFS http.FileSystem
	_, err = os.Stat("static")
	if err == nil {
		staticFS = http.Dir("static")
	} else {
		x, err := fs.Sub(static, "static")
		if err != nil {
			log.Fatal(err)
		}
		staticFS = http.FS(x)
	}

	// SSO-authenticate HTML so that the user is redirected to login page
	wolHtml := func(w http.ResponseWriter, r *http.Request) {
		file, err := staticFS.Open("wol.html")
		if err != nil {
			panic(err) // should never happen
		}
		stat, err := file.Stat()
		if err != nil {
			panic(err) // should never happen
		}
		http.ServeContent(w, r, "", stat.ModTime(), file)
	}

	// Protect against concurrent data modifications
	var lock sync.Mutex

	apiLoad := func(w http.ResponseWriter, r *http.Request) {
		lock.Lock()
		defer lock.Unlock()

		logRequestf(r, "load")

		data, err := LoadData(cfg.DataPath)
		if err != nil {
			handleError(r, w, err)
			return
		}
		user := NewUser(samlUser(r), samlEntitlements(r), cfg)

		networks := make([]string, 0) // to prevent "null" in JSON
		for _, x := range cfg.Networks {
			if user.HasAdminNetworkAccess(x.Network) {
				networks = append(networks, x.Network)
			}
		}

		type Host2 struct {
			Host
			Admin bool `json:"admin"`
		}
		hosts := make([]Host2, 0) // to prevent "null" in JSON
		for _, x := range data.Hosts {
			if user.HasNetworkAccess(x.Network) {
				admin := user.HasAdminNetworkAccess(x.Network)
				hosts = append(hosts, Host2{
					Host:  x,
					Admin: admin,
				})
			}
		}
		// Sort after network and then name
		sort.SliceStable(hosts, func(i, j int) bool {
			if hosts[i].Network < hosts[j].Network {
				return true
			} else if hosts[i].Network > hosts[j].Network {
				return false
			}
			return hosts[i].Name < hosts[j].Name
		})

		writeJson(w, map[string]interface{}{
			"user":     user.Name,
			"networks": networks,
			"hosts":    hosts,
		})
	}

	apiAdd := func(w http.ResponseWriter, r *http.Request) {
		lock.Lock()
		defer lock.Unlock()

		logRequestf(r, "add")

		if r.Method != "POST" {
			//lint:ignore ST1005 user error message
			handleError(r, w, fmt.Errorf("Method not POST"))
			return
		}

		data, err := LoadData(cfg.DataPath)
		if err != nil {
			handleError(r, w, err)
			return
		}
		user := NewUser(samlUser(r), samlEntitlements(r), cfg)
		host, ok := getApiNewHost(w, r, user.HasAdminNetworkAccess)
		if !ok {
			return
		}
		host.Id = data.NextId
		data.NextId++

		logRequestf(r, "add: host %#v", host)
		data.Hosts = append(data.Hosts, host)

		err = SaveData(cfg.DataPath, data)
		if err != nil {
			handleError(r, w, err)
			return
		}

		writeJson(w, nil)
	}

	apiWake := func(w http.ResponseWriter, r *http.Request) {
		lock.Lock()
		defer lock.Unlock()

		logRequestf(r, "wake")

		if r.Method != "POST" {
			//lint:ignore ST1005 user error message
			handleError(r, w, fmt.Errorf("Method not POST"))
			return
		}

		data, err := LoadData(cfg.DataPath)
		if err != nil {
			handleError(r, w, err)
			return
		}
		user := NewUser(samlUser(r), samlEntitlements(r), cfg)
		host, ok := getApiHostById(w, r, data, user.HasNetworkAccess)
		if !ok {
			return
		}

		network := cfg.networksMap[host.Network]

		cmd := exec.Command("wakeonlan",
			"-i", network.WolIP,
			host.MAC)
		err = cmd.Run()
		if err != nil {
			logRequestf(r, "wake: %v: %v", cmd, err)
			// Don't leak information about actual error to client
			handleError(r, w,
				fmt.Errorf("WOL failed, contact noc@fau.de"))
			return
		}
		logRequestf(r, "wake: %v", cmd)

		writeJson(w, nil)
	}

	apiDelete := func(w http.ResponseWriter, r *http.Request) {
		lock.Lock()
		defer lock.Unlock()

		logRequestf(r, "delete")

		if r.Method != "POST" {
			//lint:ignore ST1005 user error message
			handleError(r, w, fmt.Errorf("Method not POST"))
			return
		}

		data, err := LoadData(cfg.DataPath)
		if err != nil {
			handleError(r, w, err)
			return
		}
		user := NewUser(samlUser(r), samlEntitlements(r), cfg)
		host, ok := getApiHostById(w, r, data,
			user.HasAdminNetworkAccess)
		if !ok {
			return
		}

		var newHosts []Host
		for _, x := range data.Hosts {
			if x.Id == host.Id {
				continue
			}
			newHosts = append(newHosts, x)
		}
		data.Hosts = newHosts

		logRequestf(r, "delete: host %#v", host)

		err = SaveData(cfg.DataPath, data)
		if err != nil {
			handleError(r, w, err)
			return
		}

		writeJson(w, nil)
	}

	http.Handle("/", http.FileServer(staticFS))
	http.Handle("/wol.html", requireSaml(wolHtml))

	http.Handle("/api/load", requireSaml(apiLoad))
	http.Handle("/api/add", requireSaml(apiAdd))
	http.Handle("/api/wake", requireSaml(apiWake))
	http.Handle("/api/delete", requireSaml(apiDelete))
	http.Handle("/saml/", samlSP)

	err = http.ListenAndServe(cfg.Listen, nil)
	if err != nil {
		log.Fatal(err)
	}
}

// getApiHostById uses host id from an API request to load host data and
// performs permission checks.
func getApiHostById(w http.ResponseWriter, r *http.Request,
	data *Data, hasNetworkAccess func(network string) bool) (Host, bool) {

	var empty Host

	body, err := io.ReadAll(r.Body)
	if err != nil {
		handleError(r, w, err)
		return empty, false
	}
	var x struct {
		Id int `json:"hostId"`
	}
	err = json.Unmarshal(body, &x)
	if err != nil {
		handleError(r, w, err)
		return empty, false
	}

	var host Host
	var found bool
	for _, h := range data.Hosts {
		if h.Id == x.Id {
			host = h
			found = true
			break
		}
	}
	if !found {
		handleError(r, w, fmt.Errorf("Host not found"))
		return empty, false
	}

	// Permission check
	if !hasNetworkAccess(host.Network) {
		//lint:ignore ST1005 user error message
		handleError(r, w, fmt.Errorf("Permission denied"))
		return empty, false
	}

	return host, true
}

// getApiNewHost extracts host data from an API request and performs sanity
// and permission checks.
func getApiNewHost(w http.ResponseWriter, r *http.Request,
	hasNetworkAccess func(network string) bool) (Host, bool) {

	var empty Host

	body, err := io.ReadAll(r.Body)
	if err != nil {
		handleError(r, w, err)
		return empty, false
	}
	var x struct {
		Host Host `json:"host"`
	}
	err = json.Unmarshal(body, &x)
	if err != nil {
		handleError(r, w, err)
		return empty, false
	}

	// Sanity checks
	if x.Host.Network == "" {
		handleError(r, w, fmt.Errorf("Host network is empty"))
		return empty, false
	}
	if x.Host.Name == "" {
		handleError(r, w, fmt.Errorf("Host name is empty"))
		return empty, false
	}
	mac, err := net.ParseMAC(x.Host.MAC)
	if err != nil {
		handleError(r, w, fmt.Errorf("Host MAC is invalid"))
		return empty, false
	}
	// Convert to consistent form
	x.Host.MAC = mac.String()

	// Permission check
	if !hasNetworkAccess(x.Host.Network) {
		//lint:ignore ST1005 user error message
		handleError(r, w, fmt.Errorf("Permission denied"))
		return empty, false
	}

	return x.Host, true
}

// UTILITY FUNCTIONS

func writeJson(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		log.Print(err)
	}
}

func logError(r *http.Request, err error) {
	log.Printf("%q: %v", r.URL.Path, err)
}

func logRequestf(r *http.Request, format string, v ...interface{}) {
	log.Printf("%s (%s): %s", remoteAddr(r), samlUser(r),
		fmt.Sprintf(format, v...))
}

func handleError(r *http.Request, w http.ResponseWriter, err error) {
	logError(r, err)

	w.WriteHeader(http.StatusBadRequest)
	writeJson(w, map[string]string{
		"error": err.Error(),
	})
}

func remoteAddr(r *http.Request) string {
	addrs := []string{
		r.RemoteAddr,
	}

	x := r.Header.Get("x-forwarded-for")
	if x != "" {
		addrs = append(addrs, x)
	}

	return strings.Join(addrs, ",")
}
