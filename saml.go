// SAML helper functions

// Copyright (C) 2022  Simon Ruderich
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/crewjam/saml/samlsp"
)

func newSamlSP(cfg *Config) (*samlsp.Middleware, error) {
	keyPair, err := tls.LoadX509KeyPair(cfg.Saml.SPCertPath,
		cfg.Saml.SPKeyPath)
	if err != nil {
		return nil, fmt.Errorf("saml: SPCertPath %q SPKeyPath %q: %w",
			cfg.Saml.SPCertPath, cfg.Saml.SPKeyPath, err)
	}
	keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0])
	if err != nil {
		return nil, fmt.Errorf("saml: SPCert: %w", err)
	}

	idpMetadata, err := samlsp.ParseMetadata([]byte(cfg.Saml.IdpMetadata))
	if err != nil {
		return nil, fmt.Errorf("saml: IdpMetadata: %w", err)
	}
	rootURL, err := url.Parse(cfg.Saml.RootURL)
	if err != nil {
		return nil, fmt.Errorf("saml: RootURL: %w", err)
	}

	return samlsp.New(samlsp.Options{
		URL:         *rootURL,
		Key:         keyPair.PrivateKey.(*rsa.PrivateKey),
		Certificate: keyPair.Leaf,
		IDPMetadata: idpMetadata,
	})
}

func samlUser(r *http.Request) string {
	// To get all session attributes use
	// samlsp.SessionFromContext(r.Context()).(samlsp.SessionWithAttributes).GetAttributes()

	id := samlsp.AttributeFromContext(r.Context(),
		"urn:mace:dir:attribute-def:eduPersonPrincipalName") // FAU IdP
	if id == "" {
		id = samlsp.AttributeFromContext(r.Context(),
			"urn:oid:1.3.6.1.4.1.5923.1.1.1.6") // UTN IdP
	}
	// Appended by IdM for unknown reasons; remove as it's confusing
	id = strings.TrimSuffix(id, "@uni-erlangen.de")
	id = strings.TrimSuffix(id, "@utn.de")
	return id
}

func samlEntitlements(r *http.Request) []string {
	attrs := samlsp.SessionFromContext(r.Context()).(samlsp.SessionWithAttributes).
		GetAttributes()
	x := attrs["urn:mace:dir:attribute-def:eduPersonEntitlement"] // FAU IdP
	if len(x) != 0 {
		return x
	}
	x = attrs["urn:oid:1.3.6.1.4.1.5923.1.1.1.7"] // UTN IdP
	if len(x) != 0 {
		return x
	}
	return nil
}
