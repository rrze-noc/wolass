// User permissions management

// Copyright (C) 2022  Simon Ruderich
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"log"
	"os/user"
)

type permission int

const (
	permissionNone permission = iota
	permissionUser
	permissionAdmin
)

type User struct {
	Name string

	groupsMap       map[string]bool // lazily initialized via InAnyGroup()
	entitlementsMap map[string]bool
	config          *Config
}

func NewUser(name string, entitlements []string, cfg *Config) *User {
	m := make(map[string]bool)
	for _, x := range entitlements {
		m[x] = true
	}
	return &User{
		Name:            name,
		entitlementsMap: m,
		config:          cfg,
	}
}

func (u *User) HasNetworkAccess(network string) bool {
	return u.permissionForNetwork(network) >= permissionUser
}

func (u *User) HasAdminNetworkAccess(network string) bool {
	return u.permissionForNetwork(network) >= permissionAdmin
}

func (u *User) permissionForNetwork(network string) permission {
	x, ok := u.config.networksMap[network]
	if !ok {
		return permissionNone
	}

	if x.adminUsersMap[u.Name] {
		return permissionAdmin
	}
	if u.InAnyGroup(x.AdminUnixGroups) {
		return permissionAdmin
	}
	if u.HasAnyEntitlement(x.AdminEntitlements) {
		return permissionAdmin
	}

	if x.usersMap[u.Name] {
		return permissionUser
	}
	if u.InAnyGroup(x.UnixGroups) {
		return permissionUser
	}
	if u.HasAnyEntitlement(x.Entitlements) {
		return permissionUser
	}

	return permissionNone
}

func (u *User) InAnyGroup(groups []string) bool {
	if len(groups) == 0 {
		return false
	}

	if u.groupsMap == nil {
		u.groupsMap = make(map[string]bool) // != nil
		usr, err := user.Lookup(u.Name)
		if err != nil {
			log.Printf("cannot lookup user %q: %v", u.Name, err)
			return false
		}
		gids, err := usr.GroupIds()
		if err != nil {
			log.Printf("cannot lookup gids for %q: %v",
				u.Name, err)
			return false
		}
		for _, gid := range gids {
			g, err := user.LookupGroupId(gid)
			if err != nil {
				log.Printf("cannot lookup group %q (of %q): %v",
					gid, u.Name, err)
				return false
			}
			u.groupsMap[g.Name] = true
		}
	}

	for _, g := range groups {
		if u.groupsMap[g] {
			return true
		}
	}

	return false
}

func (u *User) HasAnyEntitlement(entitlements []string) bool {
	for _, x := range entitlements {
		if u.entitlementsMap[x] {
			return true
		}
	}
	return false
}
