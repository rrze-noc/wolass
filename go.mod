module wolass

go 1.22

require (
	github.com/BurntSushi/toml v1.4.0
	github.com/crewjam/saml v0.4.14
	github.com/google/renameio/v2 v2.0.0
)

require (
	github.com/beevik/etree v1.4.1 // indirect
	github.com/crewjam/httperr v0.2.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/jonboulle/clockwork v0.4.0 // indirect
	github.com/mattermost/xml-roundtrip-validator v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russellhaering/goxmldsig v1.4.0 // indirect
	golang.org/x/crypto v0.25.0 // indirect
)
