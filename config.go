// Config parsing

// Copyright (C) 2022  Simon Ruderich
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"net"

	"github.com/BurntSushi/toml"
)

type NetworkConfig struct {
	Network string // in CIDR notation

	// IdM users which can add/delete/wake hosts
	AdminUsers []string
	// Members of these unix groups can add/delete/wake hosts
	AdminUnixGroups []string
	// Entitlements which can add/delete/wake hosts
	AdminEntitlements []string

	// IdM users which can wake hosts
	Users []string
	// Members of these unix groups can wake hosts
	UnixGroups []string
	// Entitlements which can wake hosts
	Entitlements []string

	// IP to send WOL packets to. This IP must have a static ARP entry
	// with the broadcast MAC on the router, e.g. for Cisco routers `arp
	// 10.188.79.252 ffff.ffff.ffff ARPA`. Thanks to
	// https://blog.ipspace.net/2009/03/generating-layer-2-broadcast-from.html
	WolIP string

	adminUsersMap map[string]bool
	usersMap      map[string]bool
}

type Config struct {
	Listen   string // used for http.ListenAndServe
	DataPath string

	Networks []NetworkConfig

	Saml struct {
		RootURL     string
		SPCertPath  string
		SPKeyPath   string
		IdpMetadata string
	}

	networksMap map[string]NetworkConfig
}

func LoadConfig(path string) (*Config, error) {
	var cfg Config
	md, err := toml.DecodeFile(path, &cfg)
	if err != nil {
		return nil, err
	}
	undecoded := md.Undecoded()
	if len(undecoded) != 0 {
		return nil, fmt.Errorf("%q: invalid fields used: %q",
			path, undecoded)
	}

	// Sanity checks
	for _, x := range cfg.Networks {
		_, _, err := net.ParseCIDR(x.Network)
		if err != nil {
			return nil, fmt.Errorf("%q: invalid Network %q",
				path, x.Network)
		}
		ip := net.ParseIP(x.WolIP)
		if ip == nil {
			return nil, fmt.Errorf("%q: invalid WolIP %q in Network %q",
				path, x.WolIP, x.Network)
		}
	}

	// Generate maps for easy lookups
	cfg.networksMap = make(map[string]NetworkConfig)
	for i, x := range cfg.Networks {
		cfg.Networks[i].adminUsersMap = make(map[string]bool)
		for _, u := range x.AdminUsers {
			cfg.Networks[i].adminUsersMap[u] = true
		}
		cfg.Networks[i].usersMap = make(map[string]bool)
		for _, u := range x.Users {
			cfg.Networks[i].usersMap[u] = true
		}
		// At the end, copies!
		cfg.networksMap[x.Network] = cfg.Networks[i]
	}

	return &cfg, nil
}
